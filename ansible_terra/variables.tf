variable "cloud_provider" {
  type        = string
  description = "The cloud provider which will host the new infrastructure"
}

# Azurerm Provider configuration variables
variable "subscription_id" {
  type        = string
  description = "The Subscription ID which should be used. This can also be sourced from the ARM_SUBSCRIPTION_ID Environment Variable."
}

variable "tenant_id" {
  type        = string
  description = "The Tenant ID should be used. This can also be sourced from the ARM_TENANT_ID Environment Variable."
}

variable "azure_environment" {
  type        = string
  description = "The Cloud Environment which should be used. Possible values are public, usgovernment, german, and china.This can also be sourced from the ARM_ENVIRONMENT environment variable."
  default     = "usgovernment"
}

variable "client_id" {
  type        = string
  description = "he Client ID which should be used. This can also be sourced from the ARM_CLIENT_ID Environment Variable."
  default     = null
}

variable "client_secret" {
  type        = string
  sensitive   = true
  description = "The Client Secret which should be used. This can also be sourced from the ARM_CLIENT_SECRET Environment Variable."
  default     = null
}

variable "client_certificate_password" {
  type        = string
  sensitive   = true
  description = "The password associated with the Client Certificate. This can also be sourced from the ARM_CLIENT_CERTIFICATE_PASSWORD Environment Variable."
  default     = null
}

variable "client_certificate_path" {
  type        = string
  description = "The path to the Client Certificate associated with the Service Principal which should be used. This can also be sourced from the ARM_CLIENT_CERTIFICATE_PATH Environment Variable."
  default     = null
}

variable "msi_endpoint" {
  type        = string
  description = "The path to a custom endpoint for Managed Service Identity - in most circumstances, this should be detected automatically. This can also, be sourced from the ARM_MSI_ENDPOINT Environment Variable."
  default     = null
}

variable "use_msi" {
  type        = bool
  description = "Should Managed Service Identity be used for Authentication? This can also be sourced from the ARM_USE_MSI Environment Variable."
  default     = false
}

variable "vm_list" {
  type        = list(any)
  default     = []
  description = <<EOF
A list with the virtual machines you want to create.
* name -->(string)(required) Name of the virtual machine
* os -->(string)(required) The operating system you want to use, possible values "ubuntu","windows","rhel"

example:
vm_list = [
    {
      name                              = "iac-vm01",
      os                                = "ubuntu",
      vm_size                           = "Standard_D2s_v3",
      server_private_ip                 = "10.225.129.60",
      public_ip                         = true,
      disable_password_authentication   = false,
      storage_os_disk_disk_size_gb      = 48,
      storage_os_disk_managed_disk_type = "StandardSSD_LRS"
    }
  ]
EOF
}