provider "azurerm" {
  subscription_id             = var.subscription_id
  tenant_id                   = var.tenant_id
  environment                 = var.azure_environment
  client_id                   = var.client_id
  client_secret               = var.client_secret
  client_certificate_password = var.client_certificate_password
  client_certificate_path     = var.client_certificate_path
  msi_endpoint                = var.msi_endpoint
  use_msi                     = var.use_msi
  features {}
}

terraform {
  required_providers {
    azurerm = {
      source  = "hashicorp/azurerm"
      version = "2.74.0"
    }
  }
}




variable "prefix" {
  default = "andreas_test"
}

resource "azurerm_resource_group" "andreas_rg_test" {
  name     = "${var.prefix}-resources"
  location = "usgovvirginia"
}

resource "azurerm_public_ip" "example" {
  for_each            = {for s in var.vm_list : format("%s", s.name)=>s}
  name                = "${each.value.name}-ip"
  resource_group_name = azurerm_resource_group.andreas_rg_test.name
  location            = azurerm_resource_group.andreas_rg_test.location
  allocation_method   = "Static"

  tags = {
    environment = "test_andreas"
  }
}
resource "azurerm_virtual_network" "andreas_vnet_test" {
  name                = "${var.prefix}-network"
  address_space       = ["10.0.0.0/16"]
  location            = azurerm_resource_group.andreas_rg_test.location
  resource_group_name = azurerm_resource_group.andreas_rg_test.name
}

resource "azurerm_subnet" "andreas_internal_subnet_test" {
  name                 = "internal"
  resource_group_name  = azurerm_resource_group.andreas_rg_test.name
  virtual_network_name = azurerm_virtual_network.andreas_vnet_test.name
  address_prefixes     = ["10.0.2.0/24"]
}



resource "azurerm_network_interface" "andreas_neti_test" {
  for_each              = {for s in var.vm_list : format("%s", s.name)=>s}
  name                  = each.value.name
  location              = azurerm_resource_group.andreas_rg_test.location
  resource_group_name   = azurerm_resource_group.andreas_rg_test.name
 

  ip_configuration {
    name                          = "testconfiguration1"
    subnet_id                     = azurerm_subnet.andreas_internal_subnet_test.id
    public_ip_address_id = azurerm_public_ip.example[each.key].id
    private_ip_address_allocation = "Dynamic"
  }
}



resource "azurerm_virtual_machine" "andreas_vm_test" {
  for_each              = {for s in var.vm_list : format("%s", s.name)=>s}
  name                  = each.value.name
  location              = azurerm_resource_group.andreas_rg_test.location
  resource_group_name   = azurerm_resource_group.andreas_rg_test.name
  network_interface_ids = [azurerm_network_interface.andreas_neti_test[each.key].id]
  vm_size               = "Standard_DS1_v2"

  # Uncomment this line to delete the OS disk automatically when deleting the VM
  # delete_os_disk_on_termination = true

  # Uncomment this line to delete the data disks automatically when deleting the VM
  # delete_data_disks_on_termination = true

  storage_image_reference {
    publisher = "Canonical"
    offer     = "UbuntuServer"
    sku       = "16.04-LTS"
    version   = "latest"
  }
  storage_os_disk {
    name              = "${each.value.name}-osdisk"
    caching           = "ReadWrite"
    create_option     = "FromImage"
    managed_disk_type = "Standard_LRS"
  }
  os_profile {
    computer_name  = each.value.name
    admin_username = "testadmin"
    admin_password = "Password1234!"
  }
  os_profile_linux_config {
    disable_password_authentication = true
    ssh_keys {

      key_data = file("~/.ssh/id_rsa.pub")
      path     = "/home/testadmin/.ssh/authorized_keys"
    }
  }
  tags = {
    environment = "staging"
  }
}